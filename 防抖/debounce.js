let btn = document.querySelector('button'); //获取按钮
const inp = document.querySelector('input'); //获取输入框
let timerID = null; //定时器定义

//******************* 测试函数testFunc() ***************************
function testFunc(){ 
    //仅执行1次
    console.log('点击'); //按钮点击触发测试
    console.log(inp.value); //输入框触发测试

    //需要保证：指向对象本身，非window
    console.log('this=', this); 
    console.log('arguments=',arguments); 
}

//**********************按钮点击监测事件（写法一）************************************  
btn.onclick = debounce(testFunc, 1000);
//**********************按钮点击监测事件（写法二）************************************  
// btn.addEventListener('click',debounce(testFunc, 1000)); 

//**********************输入框输入监测事件（写法一）************************************  
inp.oninput= debounce(testFunc, 1000);
//**********************输入框输入监测事件（写法二）************************************  
// inp.addEventListener('input',debounce(testFunc, 1000)); 

//**********************防抖函数（写法一）************************************  
function debounce(func, delay) {
    return function () {     
       if(timerID) clearTimeout(timerID); //清除已有的定时器
        timerID= setTimeout(()=>{
            func.apply(this, arguments); //apply函数可以修改this的指向，箭头函数中可直接使用this。
        }, delay);
    }
}
//**********************防抖函数（写法二）************************************  
function debounce(func, delay) { 
    return function () { 
        //容易遗漏
        let context = this;  //保证this指向对象本身，而不是window
        let args = arguments; //清除定时器前的arguments
        if(timerID) clearTimeout(timerID); //清除已有的定时器
        timerID = setTimeout(function () {    
            func.apply(context, args); //apply函数可以修改this的指向，function函数中不能直接使用this。
           
        }, delay);
    }
}
