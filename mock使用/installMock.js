// 使用 Mock
// import { Mock } from 'mockjs';
// var Mock = require('mockjs')
// import Mock from "mockjs";

var data = Mock.mock({
    // 属性 list 的值是一个数组，其中含有 1 到 10 个元素
    'list|1-10': [{
        // 属性 id 是一个自增数，起始值为 1，每次增 1
        'id|+1': 1
    }],
    'number1|1-100.1-10': 1,
    'number2|123.1-10': 1,
    'number3|123.3': 1,
    'number4|123.10': 1.123,
    'number5|3': ["hao", "da", "ren"],
    'number6|1-5': ["hao", "da", "ren"],
    'number7|+1': ["hao", "da", "ren"],
    'number8|1': ["hao", "da", "ren"],
    'number9': function () { 
        return "函数";
    },
    'regexp1': /[a-z][A-Z][0-9]/,
    'regexp2': /\w\W\s\S\d\D/,
    'regexp3': /\d{5,10}/,
    name: {
        first: '@FIRST',
        middle: '@FIRST',
        last: '@LAST',
        full: '@first @middle @last'
    }
})
Mock.setup({
    timeout: 4000
})
// 输出结果
console.log("mock的开始与安装=", JSON.stringify(data, null, 4))
var Random = Mock.Random
Random.email()
Random.extend({
    constellation: function(date) {
        var constellations = ['白羊座', '金牛座', '双子座', '巨蟹座', '狮子座', '处女座', '天秤座', '天蝎座', '射手座', '摩羯座', '水瓶座', '双鱼座']
        return this.pick(constellations)
    }
})
Random.constellation()
// => "水瓶座"
Mock.mock('@CONSTELLATION')
// => "天蝎座"
Mock.mock({
    constellation: '@CONSTELLATION'
})