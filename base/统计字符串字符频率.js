// 统计不重复的字符
let str = "hello world";
let obj = {};
for (let k of str) { 
    if (k === ' ') continue;
    if (k in obj) {
        obj[k]++;
    } else { 
        obj[k] = 1;
    }
}
console.log("obj=", obj);
console.log("obj[l]=",obj['l']);