//箭头函数的作用域：向外层作用域中，一层层查找this，直到有最近this定义。 
setTimeout(() => {
    console.log('一般情况下，箭头函数的this指向最近的this定义=',this);   //this指向window
}, 1000);

setTimeout(function(){
    console.log('一般情况下，function函数的this指向window=',this);   //this指向window
}, 3000);

const obj = {
    aaa() { 
        console.log('某一函数的this一般指向这个函数本身=',this); //this指向当前对象。
        setTimeout(function(){
            console.log('某一函数内function函数的this一般指向window=',this);   //this指向window
        }, 4000);

        setTimeout(() => {
            console.log('某一函数内箭头函数的this指向这个函数本身=',this);   //this指向window
        }, 5000);
    }
   
}
obj.aaa();